﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Catalogue.aspx.cs" Inherits="Catalogue" %>
<%@ Register TagPrefix="obout" Namespace="Obout.Grid" Assembly="obout_Grid_NET" %>
<%@ Register TagPrefix="obout" Namespace="Obout.Interface" Assembly="obout_Interface" %>
<%@ Register TagPrefix="owd" Namespace="OboutInc.Window" Assembly="obout_Window_NET" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Библиотечный каталог</title>
    <script type="text/javascript" >

        function btnAppendBookClick() {
            txtBookName.value('');
            txtBookAuthor.value('');
            txtISBN.value('');
            document.getElementById("curBookID").value = -1;
            dlg.Open();
        }

        function btnEditBookClick(row) {
            document.getElementById("curBookID").value = gridBooks.Rows[row].Cells[0].Value;
            txtBookName.value(gridBooks.Rows[row].Cells[1].Value);
            txtBookAuthor.value(gridBooks.Rows[row].Cells[2].Value);
            txtISBN.value(gridBooks.Rows[row].Cells[3].Value);
            ddlLibraries.value(gridBooks.Rows[row].Cells[4].Value);
            dlg.Open();
        }

        function btnDeleteBookClick(row) {
            if (confirm("Вы действительно хотите удалить эту книгу из каталога?")) {
                var oRecord = new Object();
                oRecord.BookID = gridBooks.Rows[row].Cells[0].Value;
                gridBooks.executeDelete(oRecord);
            }
        }

        function btnSaveClick() {
            var oRecord = new Object();
            oRecord.BookName = txtBookName.value();
            oRecord.Author = txtBookAuthor.value();
            oRecord.ISBN = txtISBN.value();
            oRecord.LibraryID = ddlLibraries.value();
            BookID = document.getElementById("curBookID").value;
            if (BookID == -1) {
                gridBooks.executeInsert(oRecord);
            }
            else {
                oRecord.BookID = BookID;
                gridBooks.executeUpdate(oRecord);
            }
            dlg.Close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h2>Библиотечный каталог (тестовое задание)</h2>
        <a class="ob_gAL" style="cursor: pointer" onclick="btnAppendBookClick()">Добавить книгу в каталог</a>
        <br />
        <br />

            <obout:Grid runat="server" ID="gridBooks" FolderLocalization="styles/grid/localization" Language="ru" FolderStyle="styles/grid/grand_gray"
                OnDataSourceNeeded="GridBooksDataSourceNeeded" AllowPaging="false" ShowFooter="false"
                Serialize="false" CallbackMode="true" AutoGenerateColumns="false"
                OnInsertCommand="InsertBook" OnUpdateCommand="UpdateBook" OnDeleteCommand="DeleteBook"
                EnableTypeValidation="false" AllowColumnResizing="true" Width="100%">
                <ScrollingSettings ScrollHeight="700" />
                <Columns>
                    <obout:Column DataField="BookID" ReadOnly="true" Visible="false" Width="1%"></obout:Column>
                    <obout:Column DataField="BookName" HeaderText="Название книги" Wrap="true" SortOrder="Asc"></obout:Column>
                    <obout:Column DataField="Author" HeaderText="Автор" Wrap="true"></obout:Column>
                    <obout:Column DataField="ISBN" Width="100"></obout:Column>
                    <obout:Column DataField="LibraryID" Visible="false"></obout:Column>
                    <obout:Column DataField="LibraryName" HeaderText="Библиотека"></obout:Column>
                    <obout:Column HeaderText="Действие" AllowSorting="false" Width="100">
                        <TemplateSettings TemplateId="ActionTemplate" />
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="ActionTemplate">
                        <Template>
                            <%# (string.Format("<div align='center'>"
                                + "<a id=\"btnEditBook_link_{0}\" onclick=\"btnEditBookClick({0})\" class=\"ob_gAL\" style=\"cursor: pointer\">Редактировать</a>"
                                + "&nbsp;&nbsp;&nbsp;&nbsp;"
                                + "<a onclick=\"btnDeleteBookClick({0})\" class=\"ob_gAL\" style=\"cursor: pointer\">Удалить</a>"
                                + "</div>", Container.PageRecordIndex)) %>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>

            <owd:Dialog runat="server" ID="dlg" IsModal="true" Position="SCREEN_CENTER" Title="Редактирование свойств книги" StyleFolder="styles/dialog/grandgray" Width="600">
                <br />
                <input type="hidden" id="curBookID" value="-1" />
                <center>
                    <asp:Table runat="server" Width="550">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">Название</asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center"><obout:OboutTextBox runat="server" ID="txtBookName" Width="350" FolderStyle="styles/interface/grand_gray/OboutTextBox" /></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">Автор</asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center"><obout:OboutTextBox runat="server" ID="txtBookAuthor" Width="350" FolderStyle="styles/interface/grand_gray/OboutTextBox" /></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">ISBN</asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center"><obout:OboutTextBox runat="server" ID="txtISBN" Width="350" FolderStyle="styles/interface/grand_gray/OboutTextBox" /></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">Библиотека</asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center"><obout:OboutDropDownList runat="server" ID="ddlLibraries" Width="350" FolderStyle="styles/interface/grand_gray/OboutDropDownList" /></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <br />
                    <br />
                    <obout:OboutButton runat="server" ID="btnClose" Text="Закрыть" OnClientClick="dlg.Close(); return false;" Width="100" FolderStyle="styles/interface/OboutButton" />
                    <obout:OboutButton runat="server" ID="btnSave" Text="Сохранить" OnClientClick="btnSaveClick(); return false;" Width="100" FolderStyle="styles/interface/OboutButton" />
                </center>
            </owd:Dialog>

    </div>
    </form>
</body>
</html>
