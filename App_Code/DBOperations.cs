﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SQLite;
using System.Data;
using System.Web.Hosting;

public static class DBOperations
{

    static readonly string DBFileName = HostingEnvironment.ApplicationPhysicalPath + @"\bin\catalogue.db3";
    static readonly string ConnectionString = string.Format("Data Source = {0}", DBFileName);

    public static DataTable GetBooks(string SortOrder)
    {
        try
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                conn.Open();
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter("select B.id as BookID, B.Name as BookName, Author, ISBN, L.ID as LibraryID, L.Name as LibraryName from Books as B join Libraries as L on B.LibraryID=L.ID" + SortOrder, conn))
                {
                    DataTable table = new DataTable();
                    adapter.Fill(table);
                    return table;
                }
            }
        }
        catch
        {
            return null;
        }
    }

    public static DataTable GetLibraries()
    {
        try
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                conn.Open();
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter("select id, name from Libraries", conn))
                {
                    DataTable table = new DataTable();
                    adapter.Fill(table);
                    return table;
                }
            }
        }
        catch
        {
            return null;
        }
    }

    public static bool DeleteBook(int BookID)
    {
        try
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("delete from Books where ID=@ID", conn))
                {
                    cmd.Parameters.AddWithValue("@ID", BookID);
                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
        }
        catch
        {
            return false;
        }
    }

    public static bool UpdateBook(int BookID, string Name, string Author, string ISBN, int LibraryID)
    {
        try
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("update Books set Name=@Name, Author=@Author, ISBN=@ISBN, LibraryID=@LibraryID where ID=@ID", conn))
                {
                    cmd.Parameters.AddWithValue("@ID", BookID);
                    cmd.Parameters.AddWithValue("@Name", Name);
                    cmd.Parameters.AddWithValue("@Author", Author);
                    cmd.Parameters.AddWithValue("@ISBN", ISBN);
                    cmd.Parameters.AddWithValue("@LibraryID", LibraryID);
                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
        }
        catch
        {
            return false;
        }
    }

    public static bool InsertBook(string Name, string Author, string ISBN, int LibraryID)
    {
        try
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("insert into Books (Name, Author, ISBN, LibraryID) values (@Name, @Author, @ISBN, @LibraryID)", conn))
                {
                    cmd.Parameters.AddWithValue("@Name", Name);
                    cmd.Parameters.AddWithValue("@Author", Author);
                    cmd.Parameters.AddWithValue("@ISBN", ISBN);
                    cmd.Parameters.AddWithValue("@LibraryID", LibraryID);
                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
        }
        catch
        {
            return false;
        }
    }

}