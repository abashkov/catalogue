﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obout.Interface;
using Obout.Grid;
using System.Data;

public partial class Catalogue : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            FillLibraryList();
    }

    private void FillLibraryList()
    {
        using (DataTable libraries = DBOperations.GetLibraries())
        {
            ddlLibraries.DataSource = libraries;
            ddlLibraries.DataValueField = "ID";
            ddlLibraries.DataTextField = "Name";
            ddlLibraries.DataBind();
        }
    }

    protected void GridBooksDataSourceNeeded(object sender, GridDataSourceNeededEventArgs e)
    {
        string SortOrder = "";
        if (e.SortExpression != "")
            SortOrder = " ORDER BY " + e.SortExpression;
        using (DataTable books = DBOperations.GetBooks(SortOrder))
        {
            gridBooks.DataSource = books;
            gridBooks.DataBind();
        }
    }

    protected void InsertBook(object sender, GridRecordEventArgs e)
    {
        DBOperations.InsertBook(Name: e.Record["BookName"].ToString(),
            Author: e.Record["Author"].ToString(),
            ISBN: e.Record["ISBN"].ToString(),
            LibraryID: Convert.ToInt32(e.Record["LibraryID"]));
    }

    protected void UpdateBook(object sender, GridRecordEventArgs e)
    {
        DBOperations.UpdateBook(BookID: Convert.ToInt32(e.Record["BookID"]),
            Name: e.Record["BookName"].ToString(),
            Author: e.Record["Author"].ToString(),
            ISBN: e.Record["ISBN"].ToString(),
            LibraryID: Convert.ToInt32(e.Record["LibraryID"]));
    }

    protected void DeleteBook(object sender, GridRecordEventArgs e)
    {
        DBOperations.DeleteBook(Convert.ToInt32(e.Record["BookID"]));
    }

}